     <!--CONTACT US-->
    <div class="contact-us">
        <div class="container">
            <div class="contact-us-container clearfix">
                <h1 class="text-center">Mulai Mencari</h1>
                <div class="col-md-12">
                    <div class="col-md-6">
                        <a class="btn btn-light btn-block pre-approval-btn" href="@">Kalkulator Simulasi</a>
                    </div>
                    <div class="col-md-6">
                        <a class="btn btn-light btn-block join-us-btn" href="#">Bergabung bersama kami</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--CONTACT US-->

    <!--FOOTER-->
    <footer>
        <div class="container" style="margin-top: 40px;">
            <div class="row">
                <div class="col-md-3">
                    <h4 class="sembiru">Hubungi Kami</h4>
                    <p>Sahid Sudirman Center 43E floor<br>
                        Jl. Jend. Sudirman No.86, Jakarta Pusat,<br>
                        DKI Jakarta Indonesia 10220</p><br>
                    <p>
                        Telepon     : (021) 2788 9788<br>
                        Email       : cs@loanmarket.co.id<br>
                        Jam Kerja   : Senin - Jumat (09.00 - 17.00)
                    </p>    
                </div>
                <div class="col-md-3">
                    <h4 class="sembiru">Tentang Kami</h4>
                    <ul class="list-unstyled">
                        <li><a href="#">Produk</a></li>
                        <li><a href="#">Berita</a></li>
                        <li><a href="#">Karir</a></li>
                        <li><a href="#">Kenapa Loan Market?</a></li>
                        <li><a href="#">Lisensi</a></li>
                    </ul>
                </div>
                <div class="col-md-3">
                    <h4 class="sembiru">Tercatat Di</h4>
                    <h4 class="siblack">Otoritas Jasa Keuangan</h4>
                    <p>S-277/MS.72/2019</p>
                    <h4 class="sembiru">Social Media</h4>
                    <ul class="list-unstyled social-icon">
                        <li><a href="#"><i class="fab fa-facebook"></i></a></li>
                        <li><a href="#"><i class="fab fa-linkedin-in"></i></a></li>
                        <li><a href="#"><i class="fab fa-instagram"></i></a></li>
                        <li><a href="#"><i class="fab fa-twitter"></i></a></li>
                    </ul><br><br>
                    <small class="smallteks">2020 Loan Market | <a href="index.php?page=privacy-policy">Kebijakan Privasi</a> | <a href="index.php?page=syarat-ketentuan">Syarat & Ketentuan</a></small>
                </div>
                <div class="col-md-3">
                    <img src="assets/images/loan-made-simple.jpg" class="card-img-top img-fluid">
                </div>
            </div>
        </div>
    </footer>
    <!--FOOTER-->

    <script>
        window.onscroll = function() {myFunction()};
        
        var header = document.getElementById("myHeader");
        var sticky = header.offsetTop;
        
        function myFunction() {
            if (window.pageYOffset > sticky) {
            header.classList.add("sticky");
            } else {
            header.classList.remove("sticky");
            }
        }
    </script>
    <script>
        AOS.init({
            duration: 1000,
        })
    </script>
        
</body>
</html>