<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Loan Market Website New 2019</title>
    <link rel="stylesheet" media="all" href="assets/css/style.css" type="text/css">
    <link rel="stylesheet" media="all" href="assets/css/bootstrap.min.css" type="text/css">
    <link href='assets/images/favicon.png' rel='icon' sizes='16x16' type='image/png'>
    <script src="assets/js/global.js"></script>
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <script src="https://d28fs0o8ewdlxv.cloudfront.net/compare-assets/vendor-605b9512da79c45c3ba39babfe61304a.js"></script>
    <script src="https://d28fs0o8ewdlxv.cloudfront.net/compare-assets/application-8ace7e67f61e48b966d7da10c215fe42.js"></script>
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
   

    <!--FONT AWESOME-->
	
	<link href="assets/fontawesome-free-5.13.1-web/css/all.css" rel="stylesheet">

<!-- 
    <style>
        .sembiru { font-size: 1.2em; font-weight: bold; color: #48a9e0;}
        .siblack { font-size: 1.2em; font-weight: bold; color: #666666;}
        .smallteks {color: #48a9e0; font-weight: bold;}
        .garisbawah {border-bottom: 5px solid #48a9e0;}
        .social-icon {}
        .social-icon li { float: left; border: 1px solid #48a9e0; padding: 10px; border-radius: 50%; width: 43px; margin-right: 10px; text-align: center;}
    </style> -->

</head>
<body class="homepage-v2 lm loanmarket_au" itemscope itemtype="http://schema.org/Organization">
    <!--HEADER-->
    <div class="inside" id="myHeader" style="z-index: 9">
    <header class="header" style="background: white;">
        <div class="container">
            <div class="flex-container in-row between">
                <a class="logo" href="index.php?page=index"><img width="100" height="100" src="assets/images/lm-logo.png" alt="Logo" /></a>
                <div class="action-buttons flex-container in-row between">
                    <span class="smallteks"><i class="fas fa-phone-alt"></i> Hubungi Kami 0818 772 015</span> &nbsp;<a href="#" class="badge badge-primary">Sign In</a>&nbsp; &nbsp;
                    <div class="burger">
                        <span></span>
                        <span></span>
                        <span></span>
                    </div>
                </div>
            </div>
        </div>
    </header>
    <!--HEADER-->

    <!--BURGER MENU-->
    <div class="burger-menu">
        <div class="flex-container in-row row-reverse">
            <a class="close-burger active">
                <span></span>
                <span></span>
                <span></span>
            </a>
        </div>
        <ul>
            <li><a href="index.php?page=produk">Produk</a></li>
            <li><a href="index.php?page=new_cari-loan-adviser">Cari Loan Adviser</a></li>
            <li><a href="index.php?page=kalkulator" >Kalkulator Simulasi</a></li>
            <li><a href="index.php?page=kenapa-loan-market">Kenapa Loan Market?</a></li>
            <li><a href="index.php?page=bergabung-bersama-kami">Bergabung Bersama Kami</a></li>
            <li><a href="index.php?page=berita">Berita</a></li>
            <li><a href="index.php?page=kontak-kami">Kontak Kami</a></li>
        </ul>
    </div>
    <!--BURGER MENU-->

    <!--MENU-->
        <div class="header-inline-menu">
            <div class="container">
                <ul class="flex-container text-center">
                    <li><a href="index.php?page=produk">Produk</a></li>
                    <li><a href="index.php?page=new_cari-loan-adviser">Cari Loan Adviser</span></a></li>
                    <li><a href="index.php?page=kalkulator">Kalkulator Simulasi</span></a></li>
                    <li><a href="index.php?page=kenapa-loan-market">Kenapa Loan Market?</a></li>
                    <li><a href="index.php?page=bergabung-bersama-kami">Bergabung Bersama Kami</a></li>
                    <li><a href="index.php?page=berita">Berita</a></li>
                    <li><a href="index.php?page=kontak-kami">Kontak Kami</a></li>
                </ul>
            </div>
        </div>
    </div>
    <!--MENU-->